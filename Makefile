# Author: Olivier Lenoir <olivier.len02@gmail.com>
# Created: 2023-01-15 15:15:44
# License: MIT, Copyright (c) 2023 Olivier Lenoir
# Project: pdflatex

all: pdf

.PHONY: pdf

.SILENT: pdf clean


pdf: clean
	for file in *.tex; do \
		echo $${file}; \
		pdflatex --shell-escape $${file}; \
		bibtex $${file%.tex}; \
		pdflatex --shell-escape $${file}; \
		pdflatex --shell-escape $${file}; \
		make clean; \
		done


clean:
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.nav
	rm -f *.out
	rm -f *.toc
	rm -f *-gnuplottex-fig*.*
	rm -f *.gnuploterrors
